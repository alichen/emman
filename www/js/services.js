/**
 * Emman.services Module
 *
 * Description
 */
angular.module('Emman.services', ['angular-md5'])

.run(function() {
    AV.initialize("qy0eqbg062azs6dl30g9mw2kwcuxnxrme7g0zqvaypq9ms3w", "1igzxp37f0ajqnqcqlj5h83qyi6yuy0q91m7yq3ftoqje7xs");
})

.factory('Timeline', ['$http', '$q', 'md5',
    function($http, $q, md5) {
        var avos = {
            url: "https://cn.avoscloud.com/1.1/classes/Timeline",
            headers: {
                'X-AVOSCloud-Application-Id': 'qy0eqbg062azs6dl30g9mw2kwcuxnxrme7g0zqvaypq9ms3w',
                'X-AVOSCloud-Application-Key': '1igzxp37f0ajqnqcqlj5h83qyi6yuy0q91m7yq3ftoqje7xs',
            }
        };
        var models = {
            timeline: AV.Object.extend('timeline')
        };
        return {
            query: function(params) {
                return $http({
                    url: avos.url,
                    headers: avos.headers,
                    params: params,
                    cache: true,
                    method: 'get'
                });
            },
            get: function(id) {
                var url = avos.url + '/' + id;
                return $http({
                    url: url,
                    headers: avos.headers,
                    cache: true,
                    method: 'get'
                });
            },
            update: function(id, data) {
                var url = avos.url + '/' + id;
                return $http({
                    method: 'put',
                    url: url,
                    headers: avos.headers,
                    data: data
                });
            },
            save: function(item) {
                var Timeline = AV.Object.extend("Timeline"),
                    timeline = new Timeline();
                timeline.set("name", "alichen");
                timeline.set("picture", item.picture);
                timeline.set("thumbnail", item.thumbnail);
                timeline.set("content", item.content);

                return timeline.save();
            },
            uploadPicture: function(pictureURI) {
                // pictureURI = "data:image/jpeg;base64," + pictureURI
                var unixtime = new Date().getTime(),
                    fileName = unixtime + "_" + "emman.jpg",
                    file = new AV.File(fileName, {
                        base64: pictureURI
                    });
                return file.save();
            },
            takePicture: function(sourceType) {
                var options = {
                        quality: 50,
                        destinationType: Camera.DestinationType.DATA_URL,
                        // destinationType: Camera.DestinationType.FILE_URL,
                        sourceType: sourceType, // 0:Photo Library, 1=Camera, 2=Saved Photo Album
                        encodingType: 0 // 0=JPG 1=PNG
                    },
                    deferred = $q.defer();
                if (!navigator.camera) {
                    return;
                }
                navigator.camera.getPicture(function(pictureURI) {
                    deferred.resolve(pictureURI);
                    //$scope.pictureURI = pictureURI;
                }, function(err) {
                    deferred.reject(err);
                    // console.log(err);
                }, options);
                return deferred.promise;
            }
        };
    }
]);
