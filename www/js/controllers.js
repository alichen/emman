angular.module('Emman.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {
    // Form data for the login modal
    $scope.loginData = {};

    // Create the login modal that we will use later
    $ionicModal.fromTemplateUrl('templates/login.html', {
        scope: $scope
    }).then(function(modal) {
        $scope.modal = modal;
    });

    // Triggered in the login modal to close it
    $scope.closeLogin = function() {
        $scope.modal.hide();
    };

    // Open the login modal
    $scope.login = function() {
        $scope.modal.show();
    };

    // Perform the login action when the user submits the login form
    $scope.doLogin = function() {
        console.log('Doing login', $scope.loginData);

        // Simulate a login delay. Remove this and replace with your login
        // code if using a login system
        $timeout(function() {
            $scope.closeLogin();
        }, 1000);
    };
})

.controller('TimelineCtrl', function($scope, $ionicPopup, $ionicModal, Timeline) {
    var queryParams = {
        order: 'name',
        limit: 7
    };
    $scope.reqTimes = 1;
    Timeline.query(AV._.extend(queryParams, {
        skip: 0
    })).success(function(data) {
        $scope.items = data.results;
    });

    //refresh list items when pull
    $scope.refresh = function() {
        var reqTimes = $scope.reqTimes,
            skip = reqTimes * 7 + 1;
        Timeline.query(AV._.extend(queryParams, {
            order: 'name',
            limit: 7,
            skip: skip
        })).success(function(data) {
            var items = $scope.items;
            $scope.items = items.concat(data.results);
            $scope.reqTimes += 1;
        }).finally(function() {
            // Stop the ion-refresher from spinning
            $scope.$broadcast('scroll.refreshComplete');
        });
    };

    //init timeline form modal
    $ionicModal.fromTemplateUrl('templates/timeline/form.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.modal = modal;
    });
    $scope.item = {};
    $scope.create = function() {
        $scope.modal.show();
    };
    $scope.closeForm = function() {
        $scope.modal.hide();
    };
    $scope.takePicture = function(sourceType) {
        var promise = Timeline.takePicture(sourceType);
        promise.then(function(pictureURI) {
            // $scope.pictureURI = pictureURI;
            Timeline.uploadPicture(pictureURI).then(function(file) {
/*                $ionicPopup.alert({
                    title: 'success!',
                    // template: file.thumbnailURL(100, 200)
                    template: 'Upload success!'
                });*/
                var thumbnail = file.thumbnailURL(80, 60);
                $scope.item.picture = file;
                $scope.item.thumbnail = thumbnail;
                // $scope.item.thumbnail = file.thumbnailURL(100, 200);
                $scope.$apply();
            });
        }, function(err) {
            console.log(err);
        });
    };

    $scope.save = function() {
        var item = $scope.item;
        Timeline.save(item).then(function(res) {
            $ionicPopup.alert({
                title: 'success!',
                // template: res
                template: 'Timeline Save success!'
            });
            $scope.modal.hide();
        }, function(err) {
            console.log(err);
        });
    };
})

.controller('TimelineViewCtrl', function($scope, $stateParams, Timeline) {
    Timeline.get($stateParams.id).success(function(data) {
        $scope.item = data;
    });
    $scope.like = function(index) {
        var item = $scope.item,
            id = item.objectId,
            like = item.like === undefined ? 1 : item.like + 1,
            data = {
                like: like
            };
        Timeline.update(id, data).success(function(res, status) {
            $scope.item.like = like;
        });
    };
})

.controller('SettingCtrl', function($scope) {
    $scope.setting = {};
})

.controller('AboutCtrl', function($scope, $stateParams) {

});
